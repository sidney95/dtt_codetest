$(document).ready(function(){

    var hideModal = function(e) {
            var className = e.target.className;

            if(className.includes("modal-box") || className.includes("container")){
                $('.modal-box').hide();
            }
        };

    $(".close-btn").click(function(){
        $('.modal-box').hide();
    });

    $(".plus-btn").click(function(){
        $('.modal-box').show();
    });

    
    $(window).bind("click", hideModal);
    

});
